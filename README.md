Ansible Role - Gitlab runner on docker
=========

Launchs a gitlab runner on docker for a custom gitlab instance.

Requirements
------------

None.

What works
----------

This is a work in progress. Some stuff is still not ready. Some of the stuff that still doesn't work:

 - Every time the role is executed, it re-register the worker without deleting any previous instance.
 - Some weirdness handling certificates.

Role Variables
--------------

 - `docker_gitlab_runner_name`: Name of the runner. Usefull to distinguish from other runners 
    in gitlab.Defaults to `MyGitlabRunner`

 - `docker_gitlab_runner_gitlab_fqdn`: Host where gitlab runs. It needs to be accesible through
   ssl. This means the connection url will be this value prepended by `https://`.
   Defaults to `gitlab.example.org`.

 - `docker_gitlab_runner_container_version`: Container version to use for the gitlab runner.
   Defaults to `v13.9.0` right now, and will probably be bumped up soon.

 - `docker_gitlab_runner_token`: Runner registration token. You need to specify the token or 
   retrieve it from gitlab, either through the administration site in gitlab or through console.
   There is an example in the [converge](molecule/default/converge.yml) file for molecule's
   default scenario.

 - `docker_gitlab_runner_trusted_ca_path`: Path to a trusted certificate the runner should trust.
   You need to provide this and `docker_gitlab_runner_trusted_ca_host` if gitlab is served 
   through a self signed certificate, or a certificate signed by a custom certificate authority. 
   This should be the path of the certificate authority public certificate in a server accesible
   through ansible.

 - `docker_gitlab_runner_trusted_ca_host`: Host where the trusted certifica is located. You need
   to provide this and `docker_gitlab_runner_trusted_ca_path` if gitlab is served through a self
   signed certificate or a certificate signed by a custom CA. This should be the inventory name
   of the server where the public certificate is located on.

Dependencies
------------

This role does have no dependencies, but is designed to work in conjuntion with other roles
in [this gitlab group](https://gitlab.com/galvesband-ansible).

Example Playbook
----------------

A sample using other roles in [the gitlab group]() follows. It is the same used in the default
scenario in molecule.

```yml
---
- name: Prepare gitlab server
  hosts: gitlab

  vars:
    my_gitlab_version: "13.9.1-ce.0"
    
    my_gitlab_fqdn: gitlab.192-168-100-100.nip.io
    my_registry_fqdn: registry.192-168-100-100.nip.io
    my_ca_name: example-ca

  tasks:
    - name: "Build CA"
      include_role:
        name: certificate_authority
      vars: 
        ca_name: "{{ my_ca_name }}"

    - name: "Build gitlab's SSL certificate"
      include_role:
        name: ca_based_certificate
      vars:
        ca_based_certificate_fqdn: "{{ my_gitlab_fqdn }}"
        ca_based_certificate_ca_name: "{{ my_ca_name }}"
        ca_based_certificate_ca_host: gitlab

    - name: "Build registry's SSL certificate"
      include_role:
        name: ca_based_certificate
      vars:
        ca_based_certificate_fqdn: "{{ my_registry_fqdn }}"
        ca_based_certificate_ca_name: "{{ my_ca_name }}"
        ca_based_certificate_ca_host: gitlab

    - name: "Install CA certificate in OS"
      include_role:
        name: install_ca
      vars:
        install_ca_certificates:
          - ca_name: "{{ my_ca_name }}"
            path: "/srv/ca/{{ my_ca_name }}/ca.crt"
            host: gitlab

    - name: "Install Docker"
      include_role:
        name: docker

    - name: "Launch gitlab"
      include_role:
        name: "docker_gitlab"
      vars:
        gitlab_container_version: "{{ my_gitlab_version }}"
        gitlab_fqdn: "{{ my_gitlab_fqdn }}"
        gitlab_registry_fqdn: "{{ my_registry_fqdn }}"
        gitlab_reverse_proxy_mode: true
        gitlab_http_port: 8000
        gitlab_registry_port: 5050
        gitlab_fetch_trusted_ca:
          - path: "/srv/ca/{{ my_ca_name }}/ca.crt"
            name: "{{ my_ca_name }}"
            host: gitlab

    - name: "Set up reverse proxy for gitlab and registry"
      include_role:
        name: "reverse_proxy"
      vars:
        reverse_proxy_sites:
          default:
            ssl: false

          gitlab.192-168-121-38.nip.io:
            domains: 
              - "{{ my_gitlab_fqdn }}"
            upstreams:
              - { backend_address: localhost, backend_port: 8000 }
            ssl: true
            ssl_fetch_cert: true
            ssl_fetch_cert_host: gitlab
            ssl_fetch_cert_path: "/srv/certs/{{ my_gitlab_fqdn }}/cert.crt"
            ssl_fetch_cert_key_path: "/srv/certs/{{ my_gitlab_fqdn }}/cert.key"

          registry.192-168-121-38.nip.io:
            domains: 
              - "{{ my_registry_fqdn }}"
            upstreams:
              - { backend_address: localhost, backend_port: 5050 }
            ssl: true
            ssl_fetch_cert: true
            ssl_fetch_cert_host: gitlab
            ssl_fetch_cert_path: "/srv/certs/{{ my_registry_fqdn }}/cert.crt"
            ssl_fetch_cert_key_path: "/srv/certs/{{ my_registry_fqdn }}/cert.key"

- name: Prepare runner server
  hosts: runner

  vars:
    my_gitlab_runner_version: "v13.9.0"
    my_gitlab_fqdn: gitlab.192-168-100-100.nip.io
    my_registry_fqdn: registry.192-168-100-100.nip.io
    my_ca_name: example-ca

  tasks:
    - name: "Install CA certificate in OS"
      include_role:
        name: install_ca
      vars:
        install_ca_certificates:
          - ca_name: "{{ my_ca_name }}"
            path: "/srv/ca/{{ my_ca_name }}/ca.crt"
            host: gitlab

    - name: "Install Docker"
      include_role:
        name: docker

    # Wait for gitlab to be ready
    - name: "Wait for gitlab to be ready"
      uri:
        url: "https://{{ my_gitlab_fqdn }}/"
        ca_path: "/usr/local/share/ca-certificates/extra/{{ my_ca_name }}.crt"
      register: gitlab_initial_response
      delegate_to: runner
      until: gitlab_initial_response.status == 200 and gitlab_initial_response.redirected == true
      ignore_errors: yes
      retries: 360
      delay: 1

    # Obtain token
    # https://stackoverflow.com/questions/53966117/unable-to-get-gitlab-runners-registration-token-from-database/54136551#54136551
    - name: "Retrieve runner registration token from gitlab"
      become: yes
      community.docker.docker_container_exec:
        container: gitlab
        command: gitlab-rails runner -e production "puts Gitlab::CurrentSettings.current_application_settings.runners_registration_token"
      changed_when: false
      delegate_to: gitlab
      register: registration_token_result

    - name: "Include docker_gitlab_runner"
      include_role:
        name: "docker_gitlab_runner"
      vars:
        docker_gitlab_runner_name: MyRunner
        docker_gitlab_runner_gitlab_fqdn: "{{ my_gitlab_fqdn }}"
        docker_gitlab_runner_container_version: "{{ my_gitlab_runner_version }}"
        docker_gitlab_runner_token: "{{ registration_token_result.stdout }}"
        docker_gitlab_runner_trusted_ca_path: "/srv/ca/{{ my_ca_name }}/ca.crt"
        docker_gitlab_runner_trusted_ca_host: gitlab
```

License
-------

[GPL 2.0 Or later](https://spdx.org/licenses/GPL-2.0-or-later.html).
